-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 29 Lis 2018, 13:26
-- Wersja serwera: 10.1.37-MariaDB
-- Wersja PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `quiz_db`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `answer`
--

CREATE TABLE `answer` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `correct` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `answer`
--

INSERT INTO `answer` (`answer_id`, `question_id`, `content`, `correct`) VALUES
(1, 1, 'Gdańsk', 0),
(2, 1, 'Olsztyn', 0),
(3, 1, 'Warszawa', 1),
(4, 1, 'Berlin', 0),
(5, 2, 'Jan', 0),
(6, 2, 'Adam', 1),
(7, 2, 'Andrzej', 0),
(8, 3, 'Kalafior', 0),
(9, 3, 'Majonez', 0),
(10, 3, 'Kartofle', 1),
(11, 3, 'Pyry', 1),
(12, 4, 'Bolesław Chrobry', 1),
(13, 4, 'Konrad Mazowiecki', 0),
(14, 4, 'Władysław Jagiełło', 1),
(15, 4, 'Henryk VIII', 0),
(16, 5, '1410', 1),
(17, 5, '1310', 0),
(18, 5, '1440', 0),
(19, 6, 'Dwóch', 0),
(20, 6, 'Jedenastu', 0),
(21, 6, 'Siedmiu', 1),
(22, 7, 'Maryla Rodowicz', 1),
(23, 7, 'Anna Jantar', 0),
(24, 7, 'Kayah', 0),
(25, 8, 'Poznań', 0),
(26, 8, 'Kraków', 1),
(27, 8, 'Rzeszów', 0),
(28, 8, 'Szczecin', 0),
(29, 9, 'Robert Kubica', 1),
(30, 9, 'Sebastien Loeb', 0),
(31, 9, 'Colin McRae', 0),
(32, 9, 'Sebastian Vettel', 1),
(33, 9, 'Daniel Ricciardo', 1),
(34, 10, 'Barszcz z uszkami', 1),
(35, 10, 'Kutia', 1),
(36, 10, 'Kotlety schabowe', 0),
(37, 10, 'Kutia', 1),
(38, 10, 'Pizza', 0),
(39, 11, 'Władca Pierścieni: Powrót króla', 1),
(40, 11, 'Titanic', 1),
(41, 11, 'Ben-Hur', 1),
(42, 11, 'Lot nad kukułczym gniazdem', 0),
(43, 12, 'PlayStation', 0),
(44, 12, 'Wii', 0),
(45, 12, 'Xbox', 1),
(46, 13, 'Ernest Hemingway', 0),
(47, 13, 'Joseph Conrad', 0),
(48, 13, 'Betty Mahmoody', 0),
(49, 13, 'Francis Scott Fitzgerald', 1),
(50, 14, '5', 1),
(51, 14, '4', 0),
(52, 14, '3', 0),
(53, 15, 'PRV-1', 0),
(54, 15, 'CCR-5', 1),
(55, 15, 'Hsp90', 0),
(56, 16, 'Shin’ya Yamanaka', 1),
(57, 16, 'James Watson', 1),
(58, 16, 'Max Born', 1),
(59, 16, 'Wolfgang Pauli', 1),
(60, 16, 'Theodor Svedberg', 1),
(61, 17, 'Odyn', 1),
(62, 17, 'Baldur', 1),
(63, 17, 'Linda', 0),
(64, 17, 'Garang', 0),
(65, 18, '1999 - 2006', 0),
(66, 18, '1991 - 1998', 1),
(67, 18, '1995 - 2002', 0),
(68, 18, '1989 - 1996', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `difficulty`
--

CREATE TABLE `difficulty` (
  `difficulty_id` int(11) NOT NULL,
  `difficulty_name` varchar(75) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `difficulty`
--

INSERT INTO `difficulty` (`difficulty_id`, `difficulty_name`) VALUES
(1, 'Dla początkujących'),
(2, 'Dla zaawansowanych'),
(3, 'Dla ekspertów');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `question`
--

CREATE TABLE `question` (
  `question_id` int(11) NOT NULL,
  `difficulty_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `is_singleor_multi` tinyint(1) NOT NULL,
  `is_valid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `question`
--

INSERT INTO `question` (`question_id`, `difficulty_id`, `content`, `active`, `created`, `is_singleor_multi`, `is_valid`) VALUES
(1, 1, 'Które miasto jest stolicą Polski?', 1, NOW(), 0, 1),
(2, 1, 'Jak miał na imię Adam Mickiewicz?', 1, NOW(), 0, 1),
(3, 1, 'Inaczej ziemniaki:', 1, NOW(), 1, 1),
(4, 1, 'Zaznacz królów Polski:', 1, NOW(), 1, 1),
(5, 1, 'W którym roku odbyła się bitwa pod Grunwaldem?', 1, NOW(), 0, 1),
(6, 1, 'Ilu było krasnoludków w bajce o Królewnej Śnieżce?', 0, NOW(), 0, 1),
(7, 2, 'Kto jest wykonawcą piosenki \"Małgośka\"?', 1, NOW(), 0, 1),
(8, 2, 'Gdzie znajduje się obraz \"Dama z gronostajem\"?', 1, NOW(), 0, 1),
(9, 2, 'Kto jest kierowcą Formuły 1?', 1, NOW(), 1, 1),
(10, 2, 'Zaznacz dania wigilijne:', 1, NOW(), 1, 1),
(11, 2, 'Które filmy zdobyły najwięcej Oscarów (11 statuetek)?', 1, NOW(), 1, 1),
(12, 2, 'Jak nazywa się konsola produkowana przez Microsoft?', 0, NOW(), 0, 1),
(13, 3, 'Kto jest autorem książki \"Wielki Gatsby\"?', 1, NOW(), 0, 1),
(14, 3, 'Ile tytułów mistrza świata zdobył Lewis Hamilton? (stan na 2018)', 1, NOW(), 0, 1),
(15, 3, 'Mutacje w którym genie wywołują odporność na zakażenie wirusem HIV?', 1, NOW(), 0, 1),
(16, 3, 'Kto z poniższych naukowców otrzymał nagrodę Nobla?', 1, NOW(), 1, 1),
(17, 3, 'Kto należy do bogów nordyckich?', 1, NOW(), 1, 1),
(18, 3, 'W jakich latach rozgrywa się akcja serii Harry Potter?', 0, NOW(), 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_quiz`
--

CREATE TABLE `user_quiz` (
  `user_quiz_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `IDX_DADD4A251E27F6BF` (`question_id`);

--
-- Indeksy dla tabeli `difficulty`
--
ALTER TABLE `difficulty`
  ADD PRIMARY KEY (`difficulty_id`);

--
-- Indeksy dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indeksy dla tabeli `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `IDX_B6F7494EFCFA9DAE` (`difficulty_id`);

--
-- Indeksy dla tabeli `user_quiz`
--
ALTER TABLE `user_quiz`
  ADD PRIMARY KEY (`user_quiz_id`),
  ADD KEY `IDX_DE93B65BAA334807` (`answer_id`),
  ADD KEY `IDX_DE93B65BA76ED395` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `answer`
--
ALTER TABLE `answer`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT dla tabeli `difficulty`
--
ALTER TABLE `difficulty`
  MODIFY `difficulty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `question`
--
ALTER TABLE `question`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT dla tabeli `user_quiz`
--
ALTER TABLE `user_quiz`
  MODIFY `user_quiz_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `FK_DADD4A251E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`question_id`);

--
-- Ograniczenia dla tabeli `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_B6F7494EFCFA9DAE` FOREIGN KEY (`difficulty_id`) REFERENCES `difficulty` (`difficulty_id`);

--
-- Ograniczenia dla tabeli `user_quiz`
--
ALTER TABLE `user_quiz`
  ADD CONSTRAINT `FK_DE93B65BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_DE93B65BAA334807` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`answer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
