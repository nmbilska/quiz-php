<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-21
 * Time: 14:49
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'label' => 'Tresc pytania',
                'attr' => array('cols' => '11', 'rows' => '7'),
                'required' => true
            ])
            ->add('difficulty', EntityType::class, [
                'class' => 'AppBundle\Entity\Difficulty',
                'placeholder' => 'Wybierz',
                'label' => 'Trudnosc pytania',
                'required' => true
            ])
            ->add('isSingleOrMulti', ChoiceType::class, [
                'label' => 'Typ pytania',
                'placeholder' => 'Wybierz',
                'choices' => array('Wielokrotnego wyboru' => true, 'Jednokrotnego wyboru' => false),
                'required' => true
            ])
            ->add('active', ChoiceType::class, [
                'label' => 'Czy pytanie jest aktywne',
                'placeholder' => 'Wybierz',
                'choices' => array('Tak' => true, 'Nie' => false),
                'required' => true
            ])
            ->add('Wyslij', SubmitType::class);

        $options = [];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => \AppBundle\Entity\Question::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_question_form';
    }
}