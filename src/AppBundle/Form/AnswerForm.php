<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-22
 * Time: 12:16
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'label' => 'Tresc odpowiedzi',
                'attr' => array('cols' => '11', 'rows' => '7'),
                'required' => true
            ])
            ->add('correct', ChoiceType::class, [
                'label' => 'Czy odpowiedz jest prawidlowa',
                'placeholder' => 'Wybierz',
                'choices' => array('Tak' => true, 'Nie' => false),
                'required' => true
            ])
            ->add('Wyslij', SubmitType::class);

        $options = [];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => \AppBundle\Entity\Answer::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_answer_form';
    }

}