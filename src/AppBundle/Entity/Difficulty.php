<?php

namespace AppBundle\Entity;

/**
 * Difficulty
 */
class Difficulty
{
    /**
     * @var int
     */
    private $difficulty_id;

    /**
     * @var string
     */
    private $difficulty_name;


    /**
     * Get difficultyId.
     *
     * @return int
     */
    public function getDifficultyId()
    {
        return $this->difficulty_id;
    }

    /**
     * Set difficultyName.
     *
     * @param string $difficultyName
     *
     * @return Difficulty
     */
    public function setDifficultyName($difficultyName)
    {
        $this->difficulty_name = $difficultyName;

        return $this;
    }

    /**
     * Get difficultyName.
     *
     * @return string
     */
    public function getDifficultyName()
    {
        return $this->difficulty_name;
    }

    public function __toString()
    {
        return $this->difficulty_name;
    }
}
