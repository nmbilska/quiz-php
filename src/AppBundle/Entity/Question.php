<?php

namespace AppBundle\Entity;

/**
 * Question
 */
class Question
{
    /**
     * @var int
     */
    private $question_id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var bool
     */
    private $isSingleorMulti;

    /**
     * @var bool
     */
    private $isValid = 0;

    /**
     * @var \AppBundle\Entity\Difficulty
     */
    private $difficulty;


    /**
     * Get questionId.
     *
     * @return int
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Question
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Question
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Question
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set isSingleorMulti.
     *
     * @param bool $isSingleorMulti
     *
     * @return Question
     */
    public function setIsSingleorMulti($isSingleorMulti)
    {
        $this->isSingleorMulti = $isSingleorMulti;

        return $this;
    }

    /**
     * Get isSingleorMulti.
     *
     * @return bool
     */
    public function getIsSingleorMulti()
    {
        return $this->isSingleorMulti;
    }

    /**
     * Set isValid.
     *
     * @param bool $isValid
     *
     * @return Question
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid.
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set difficulty.
     *
     * @param \AppBundle\Entity\Difficulty $difficulty
     *
     * @return Question
     */
    public function setDifficulty(\AppBundle\Entity\Difficulty $difficulty)
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Get difficulty.
     *
     * @return \AppBundle\Entity\Difficulty
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }
}
