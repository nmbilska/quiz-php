<?php

namespace AppBundle\Entity;

/**
 * UserQuiz
 */
class UserQuiz
{
    /**
     * @var int
     */
    private $user_quiz_id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \AppBundle\Entity\Answer
     */
    private $answer;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get userQuizId.
     *
     * @return int
     */
    public function getUserQuizId()
    {
        return $this->user_quiz_id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return UserQuiz
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set answer.
     *
     * @param \AppBundle\Entity\Answer $answer
     *
     * @return UserQuiz
     */
    public function setAnswer(\AppBundle\Entity\Answer $answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return \AppBundle\Entity\Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserQuiz
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
