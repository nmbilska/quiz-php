<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-25
 * Time: 13:59
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class AnswerRepository extends EntityRepository
{
    /**
     * @param int $questionId
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isQuestionValid(int $questionId): bool
    {
        //aby pytanie bylo wykorzystane w quizie musi miec przynajmniej dwie odpowiedzi w tym jedna poprawna

        $query = $this->getEntityManager()->createQuery(
            'SELECT COUNT (a.answer_id) FROM AppBundle:Answer a WHERE a.question = :question_id'
        );
        $query->setParameter('question_id', $questionId);

        $answers_count = $query->getSingleScalarResult();
        if ($answers_count < 2) {
            return 0;
        }

        $query = $this->getEntityManager()->createQuery(
            'SELECT COUNT (a.answer_id) FROM AppBundle:Answer a WHERE a.question = :question_id AND a.correct = 1'
        );
        $query->setParameter('question_id', $questionId);

        $correct_answers_count = $query->getSingleScalarResult();
        if (0 == $correct_answers_count) {
            return 0;
        }

        return 1;

    }

    /**
     * @param array $questions
     * @return array
     */
    public function getIdsRightAnswers(array $questions)
    {
        $rightAnswers = [];

        foreach ($questions as $question) {
            $query = $this->getEntityManager()->createQuery(
                'SELECT a.answer_id FROM AppBundle:Answer a WHERE a.question = :question AND a.correct = true'
            );
            $query->setParameter('question', $question->getQuestionId());

            $result = $query->getScalarResult();
            $rightAnswers[] = array_column($result, "answer_id");
        }

        return $rightAnswers;
    }

    public function getAnswersByQuestions(array $questions)
    {
        $rightAnswers = [];

        foreach ($questions as $question) {

            $query = $this->getEntityManager()->createQuery(
                'SELECT a FROM AppBundle:Answer a WHERE a.question = :question'
            );
            $query->setParameter('question', $question->getQuestionId());
            $rightAnswers[] = $query->getResult();
        }
        $rightAnswers = array_reduce($rightAnswers, 'array_merge', array());

        return $rightAnswers;
    }
}