<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-26
 * Time: 11:34
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{

    protected function getRandomIds(int $difficulty, int $questionsCount)
    {
        $sql =
            'SELECT question_id
            FROM question q
            WHERE q.difficulty_id = :difficulty
            AND q.active = true
            AND q.is_valid = true
            ORDER BY RAND()
            LIMIT :questionsCount';


        $statement = $this->getEntityManager()->getConnection()->prepare($sql);
        $statement->bindValue('difficulty', $difficulty);
        $statement->bindValue('questionsCount', $questionsCount, \PDO::PARAM_INT);
        $statement->execute();

        $random_ids = array();
        while ($val = $statement->fetch()) {
            $random_ids[] = $val['question_id'];
        }

        return $random_ids;
    }

    public function getRandomQuestions(int $difficulty, int $questionsCount){

        $random_ids = $this->getRandomIds($difficulty, $questionsCount);

        $questions = $this->createQueryBuilder('r')
            ->select('q')
            ->from('AppBundle:Question', 'q')
            ->where('q.question_id IN (:ids)')
            ->setParameter('ids', $random_ids)
            ->getQuery()
            ->getResult();

        return $questions;

    }


//  inne rozwiazanie na uzyskanie losowych pytan:
//  nie działa ponieważ nie zwraca dokładnej liczby pytań, funkcja ustawia max. liczbę pytań jaka może być wylosowana
//
//    public function getRandomQuestions(int $difficulty)
//    {
//        $questions = $this->createQueryBuilder('r')
//            ->from('AppBundle:Question', 'q')
//            ->where('q.difficulty = :difficulty', 'q.active = true', 'q.isValid = true')
//            ->addSelect('RAND() as HIDDEN rand')
//            ->orderBy('rand')
//            ->setParameter('difficulty', $difficulty)
//            ->getQuery()
//            ->setMaxResults(3)
//            ->getResult();
//
//        return $questions;
//    }




}