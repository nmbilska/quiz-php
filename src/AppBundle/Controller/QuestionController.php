<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-21
 * Time: 13:16
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Form\QuestionForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionController extends Controller
{

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {

        /** @var Question[] $questions */
        $questions = $this->getDoctrine()->getRepository(Question::class)->findAll();

        //Paginacja pytan - skorzystanie z paczki KnpPaginatorBundle

        /** @var \KNP\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $questions,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('Question/index.html.twig', [
            'questions' => $result
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param int $id
     * @return Response
     */
    public function readAction(int $id): Response
    {

        $question = $this->getDoctrine()->getRepository(Question::class)->findOneBy(['question_id' => $id]);

        if (empty($question)) {
            $this->addFlash('error', 'Nie ma pytania o podanym numerze id!');
            return $this->redirectToRoute('question_list');
        }

        //Pobranie daty utworzenia pytania i przedstawienie jej w postaci stringa d/m/y
        $date = $question->getCreated();
        $date = $date->format('d/m/Y');

        /** @var Answer[] $answers */
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['question' => $id]);

        return $this->render('Question/read.html.twig', [
            'question' => $question,
            'id' => $id,
            'created' => $date,
            'answers' => $answers
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        $question = new Question();
        $questionForm = $this->createForm(QuestionForm::class, $question);
        $questionForm->handleRequest($request);

        if ($questionForm->isSubmitted() && $questionForm->isValid()) {
            $now = new \DateTime();
            $question->setCreated($now);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($question);
            $entityManager->flush();
            $this->addFlash('success', 'Pytanie zostalo poprawnie dodane!');
            return $this->redirectToRoute('question_list');
        }

        return $this->render('question/new.html.twig', [
            'question_form' => $questionForm->createView(),
            'pageTitle' => 'Dodaj pytanie'
        ]);
    }


    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateAction(Request $request, int $id): Response
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);

        if (empty($question)) {
            $this->addFlash('error', 'Nie ma pytania o podanym id!');
            return $this->redirectToRoute('question_list');
        }

        $questionForm = $this->createForm(QuestionForm::class, $question);
        $questionForm->handleRequest($request);

        if ($questionForm->isSubmitted() && $questionForm->isValid()) {

            //Sprawdzenie czy pytanie nie posiada dwóch poprawnych odpowiedzi
            //W przypadku zmiany typu pytania na jednokrotnego wyboru pojawi się komunikat
            $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['question' => $id]);
            foreach ($answers as $q_answer) {
                if (
                    $question->getIsSingleorMulti() == 0 &&
                    $q_answer->getCorrect() == 1 &&
                    $question->getIsSingleorMulti() == 0
                ) {
                    $this->addFlash('error', 'Pytanie nie może mieć dwóch poprawych odpowiedzi!');
                    return $this->redirectToRoute('question_details', ['id' => $id]);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($question);
            $entityManager->flush();
            $this->addFlash('success', 'Pytanie zostało zmienione!');

            return $this->redirectToRoute('question_list');
        }

        return $this->render('question/new.html.twig', [
            'question_form' => $questionForm->createView(),
            'pageTitle' => 'Edytuj pytanie'
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(int $id)
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($id);
        if (empty($question)) {
            $this->addFlash('error', 'Nie ma pytania o podanym id!');
            return $this->redirectToRoute('question_list');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($question);
        $entityManager->flush();
        $this->addFlash('success', 'Pytanie zostało usunięte!');

        return $this->redirectToRoute('question_list');
    }

}