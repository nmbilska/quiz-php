<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-25
 * Time: 13:05
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Difficulty;
use AppBundle\Entity\Question;
use AppBundle\Entity\UserQuiz;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class QuizController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function startAction(Request $request): Response
    {
        if ($request->getMethod() == 'POST') {
            $quizDifficulty = $request->request->get('difficulty');
            $questionsCount = $request->request->get('questions_count');

            $session = new Session();
            $session->set('difficulty', $quizDifficulty);
            $session->set('questions_count', $questionsCount);
            $session->set('questions_array', []);
            $session->set('answers_array', []);

            return $this->prepareQuizAction($request);
        }
        $difficulties = $this->getDoctrine()->getRepository(Difficulty::class)->findAll();

        return $this->render('quiz/startQuiz.html.twig', [
            'difficulties' => $difficulties
        ]);
    }

    /**
     * @return Response
     */
    public function prepareQuizAction(): Response
    {
        $questionsCount = $this->get('session')->get('questions_count');
        $difficulty = $this->get('session')->get('difficulty');
        $qr = $this->getDoctrine()->getManager()->getRepository(Question::class);

        $allQuestions = $qr->count([
            'difficulty' => $difficulty,
            'active' => true,
            'isValid' => true
        ]);

        //20 pytan to maksymalna liczba jaka uzytkownik moze podac - zabezpieczenie przed obciazeniem bazy danych
        if ($questionsCount > $allQuestions || $questionsCount >= 20) {
            $this->addFlash('error', 'Podana liczba pytan jest za duza!');

            return $this->redirectToRoute('start_quiz');
        }

        $questionsQuiz = $qr->getRandomQuestions($difficulty, $questionsCount);
        $session = $this->get('session');
        $session->set('questions_array', $questionsQuiz);

        return $this->redirectToRoute('question_quiz', ['id' => 0]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function questionAction(Request $request, int $id): Response
    {
        $session = $this->get('session');

        if ($request->getMethod() == 'POST') {
            //pobieranie tablicy zaw. id odpowiedzi uzytkownika z formularza dla danego pytania
            $answer = $request->request->get('answer');
            $answerQuiz = $session->get('answers_array');
            $answerQuiz[$id] = $answer;
            $session->set('answers_array', $answerQuiz);

            if ($request->request->has('next')) {
                return $this->redirectToRoute('question_quiz', ['id' => ++$id]);
            }
            if ($request->request->has('end')) {
                return $this->redirectToRoute('result_quiz');
            }
            return $this->redirectToRoute('question_quiz', ['id' => --$id]);
        }

        //zabezpieczenie przed recznym wpisaniem adresu (np. /quiz/9999)
        try {
            $question = $session->get('questions_array')[$id];
        } catch (\Exception $exception) {
            $this->addFlash('error', 'Bledny adres URL');
            return $this->redirectToRoute('start_quiz');
        }

        //pobieranie danych (odpowiedzi, typ pytania, liczba pytan) do wygenerowania strony
        $questionId = $question->getQuestionId();
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['question' => $questionId]);
        $questionType = $question->getIsSingleOrMulti();
        $questionsCount = $session->get('questions_count');

        //pobranie odpowiedzi zaznaczonych przez uzytkownika
        try {
            $checkedAnswers = $session->get('answers_array')[$id];
        } catch (\Exception $exception) {
            $checkedAnswers = [];
        }

        return $this->render('quiz/question.html.twig', [
            'answers' => $answers,
            'question' => $question,
            'type' => $questionType,
            'id' => $id,
            'questions_count' => $questionsCount,
            'checked_answers' => $checkedAnswers
        ]);
    }

    /**
     * @return Response
     */
    public function resultAction(): Response
    {
        $session = $this->get('session');
        $questionsCount = $session->get('questions_count');
        $userAnswersId = $session->get('answers_array');

        //zabezpieczenie przed wejsciem na strone przed rozpoczeciem quizu
        if (!$userAnswersId) {
            $this->addFlash('error', 'Quiz nie jest rozpoczety!');
            return $this->redirectToRoute('start_quiz');
        }

        //zabezpieczenie przed wejsciem na adres /result zanim uzytkownik odpowie na wszystkie pytania
        try {
            $userAnswersId[$questionsCount - 1];
        } catch (\Exception $exception) {
            $this->addFlash('error', 'Nie odpowiedziales na wszystkie pytania!');
            return $this->redirectToRoute('question_quiz', ['id' => 0]);
        }

        $ar = $this->getDoctrine()->getRepository(Answer::class);
        $questions = $session->get('questions_array');
        $rightAnswersId = $ar->getIdsRightAnswers($questions);

        $points = 0;
        $correctQuestions = [];

        //obliczanie ile jest poprawnych pytan w tescie, zapisywanie ich do nowej tablicy
        for ($i = 0; $i < $questionsCount; $i++) {
            if ($userAnswersId[$i] == $rightAnswersId[$i]) {
                $correctQuestions[] = $questions[$i];
                $points++;
            }
        }
        $result = round(($points / $questionsCount) * 100);

        //zredukowanie wielowymiarowej tablicy do jednowymiarowej tablicy
        //zawiera id odpowiedzi uzytkownika
        $reducedUserAnswersId = array_reduce($userAnswersId, 'array_merge', array());
        $userAnswers = $ar->findBy(array('answer_id' => $reducedUserAnswersId));

        //pobranie wszystkich odpowiedzi do pytan z testu
        $allAnswers = $ar->getAnswersByQuestions($questions);

        //zapisywanie odpowiedzi uzytkownika w bazie danych
        //jesli uzytkownik nie byl zalogowany to odpowiedzi nie sa zapisywane
        $user = $this->getUser();

        if (!empty($user)) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($userAnswers as $answer) {
                $userQuiz = new UserQuiz();
                $userQuiz->setUser($user);
                $userQuiz->setAnswer($answer);
                $userQuiz->setCreated(new \DateTime());
                $entityManager->persist($userQuiz);
            }
            $entityManager->flush();
        }

        $session->clear();

        return $this->render('quiz/result.html.twig', [
            'questions' => $questions,
            'answers' => $allAnswers,
            'correct_questions' => $correctQuestions,
            'user_answers' => $userAnswers,
            'result' => $result
        ]);

    }
}
