<?php
/**
 * Created by PhpStorm.
 * User: B.K. Łażan
 * Date: 2018-11-22
 * Time: 12:16
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Form\AnswerForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AnswerController extends Controller
{
    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param int $question_id
     * @return Response
     */
    public function addAction(Request $request, int $question_id): Response
    {
        $question = $this->getDoctrine()->getRepository(Question::class)->find($question_id);

        if (empty($question)) {
            $this->addFlash('error', 'Nie ma pytania o podanym id!');
            return $this->redirectToRoute('question_list');
        }

        $answer = new Answer();
        $answerForm = $this->createForm(AnswerForm::class, $answer);
        $answerForm->handleRequest($request);

        if ($answerForm->isSubmitted() && $answerForm->isValid()) {

            //Sprawdzenie czy pytanie nie jest jednokrotnego wyboru i czy posiada już prawidłowe odpowiedzi
            // (może lepiej rozbić warunki?)

            $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['question' => $question_id]);
            foreach ($answers as $q_answer) {
                if (
                    $question->getIsSingleorMulti() == 0 &&
                    $q_answer->getCorrect() == 1 &&
                    $answer->getCorrect() == 1
                ) {
                    $this->addFlash('error', 'Pytanie nie może mieć dwóch poprawych odpowiedzi!');
                    return $this->redirectToRoute('question_details', ['id' => $question_id]);
                }
            }

            $answer->setQuestion($question);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($answer);
            $entityManager->flush();

            //sprawdzenie czy pytanie może być wykorzystane w quizie
            //nadanie mu odpowiedniej flagi
            $questionValidation = $this->getDoctrine()->getRepository(Answer::class)->isQuestionValid($question_id);
            $question->setIsValid($questionValidation);
            $entityManager->persist($question);
            $entityManager->flush();

            $this->addFlash('success', 'Odpowiedź została poprawnie dodana!');
            return $this->redirectToRoute('question_details', ['id' => $question_id]);
        }

        return $this->render('answer/new.html.twig', [
            'answer_form' => $answerForm->createView(),
            'pageTitle' => 'Dodaj odpowiedz',
            'question_id' => $question_id
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param int $question_id
     * @param int $id
     * @return Response
     */
    public function updateAction(Request $request, int $question_id, int $id): Response
    {
        $ar = $this->getDoctrine()->getRepository(Answer::class);
        $answer = $ar->findOneBy(['answer_id' => $id, 'question' => $question_id]);

        //zabezpieczenie przed recznym podaniem adresu (np. /answer/999/update/999
        if (empty($answer)) {
            $this->addFlash('error', 'Nie ma odpowiedzi o podanym id dla danego pytania!');
            return $this->redirectToRoute('question_list');
        }

        $answerForm = $this->createForm(AnswerForm::class, $answer);
        $answerForm->handleRequest($request);

        if ($answerForm->isSubmitted() && $answerForm->isValid()) {

            //Sprawdzenie czy pytanie nie jest jednokrotnego wyboru i czy posiada już prawidłowe odpowiedzi
            // (może lepiej rozbić warunki?)

            $question = $answer->getQuestion();
            $answers = $ar->findBy(['question' => $question_id]);
            foreach ($answers as $q_answer) {
                if (
                    $question->getIsSingleorMulti() == 0 &&
                    $q_answer->getCorrect() == 1 &&
                    $answer->getCorrect() == 1
                ) {
                    $this->addFlash('error', 'Pytanie nie może mieć dwóch poprawych odpowiedzi!');
                    return $this->redirectToRoute('question_details', ['id' => $question_id]);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($answer);
            $entityManager->flush();

            //Sprawdzenie czy pytanie może być wykorzystane w quizie
            $questionValidation = $ar->isQuestionValid($question_id);
            $question->setIsValid($questionValidation);
            $entityManager->persist($question);
            $entityManager->flush();

            $this->addFlash('success', 'Odpowiedz została zmieniona!');
            return $this->redirectToRoute('question_details', ['id' => $question_id]);
        }

        return $this->render('answer/new.html.twig', [
            'answer_form' => $answerForm->createView(),
            'pageTitle' => 'Edytuj pytanie',
            'question_id' => $question_id
        ]);
    }


    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @param int $id
     * @param int $question_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(int $id, int $question_id)
    {
        $ar = $this->getDoctrine()->getRepository(Answer::class);
        $answer = $ar->findOneBy(['answer_id' => $id, 'question' => $question_id]);

        if (empty($answer)) {
            $this->addFlash('error', 'Nie ma odpowiedzi o podanym id dla danego pytania!');
            return $this->redirectToRoute('question_details', ['id' => $question_id]);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($answer);
        $entityManager->flush();

        //Sprawdzenie czy pytanie może być wykorzystane w quizie
        $question = $this->getDoctrine()->getRepository(Question::class)->find($question_id);
        $questionValidation = $ar->isQuestionValid($question_id);
        $question->setIsValid($questionValidation);
        $entityManager->persist($question);
        $entityManager->flush();

        $this->addFlash('success', 'Odpowiedz została usunięta!');

        return $this->redirectToRoute('question_details', ['id' => $question_id]);
    }
}