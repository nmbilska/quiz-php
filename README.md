# Aplikacja - Quiz

Aplikacja reprezentująca formę quizu, obejmująca panel administratora, a także sam quiz. W panelu administratora użytkownik ma możliwość przeglądania, dodawania, edytowania i usuwania pytań i odpowiedzi. Podczas quizu pytania o wybranym poziomie trudności są losowane z bazy danych, po zakończeniu rozgrywki użytkownik może przejrzeć swoje odpowiedzi i sprawdzić ich poprawność. Quiz można rozwiązywać jako użytkownik zalogowany bądź jako gość. Odpowiedzi zalogowanego użytkownika są zapisywane w bazie danych. Aplikacja została napisana w języku polskim.

## Technologie

Podczas tworzenia projektu skorzystano z następujących technologii:
* [PHP 7.2](http://php.net/) (framework: [Symfony 3.4.18](https://symfony.com/))
* MySQL ([XAMPP](https://www.apachefriends.org/pl/index.html))
* HTML5 + CSS + jQuery (szablon pochodzi z strony [free-css.com](https://www.free-css.com/free-css-templates/page209/rs-html-120))

## Instalacja

* Wymagania: PHP + Composer 
* Zainstaluj wszystkie potrzebne paczki: ```composer install```
* Stworz bazę danych: ```php bin/console doctrine:database:create``` (odpowiednio skonfiguruj plik app/config/parameters.yml)
* Zaimportuj dane do bazy danych z pliku sql/database.sql (zawiera po 5 pytań do każdego poziomu trudności wraz z odpowiedziami) - nazwa bazy: quiz_db, host: localhost
* Uruchom serwer internetowy ```php bin/console server:run```
* Wejdz na adres podany w konsoli

## Utworzenie nowego użykownika z uprawnieniami administartora

* Utworzenie nowego uzytkownika ```php bin/console fos:user:create "nazwa_uzytkownika"```
* Nadanie uprawnień administratora wybranemu użytkownikowi: ```php bin/console fos:user:promote "nazwa_uzytkownika" ROLE_ADMIN```










